import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/")
public class BookStoreApplication extends ResourceConfig {
    public BookStoreApplication() {
        packages("filters");
        packages("controllers");
    }
}
