package services;

import domain.models.Book;
import repositories.BookStoreRepository;
import repositories.interfaces.IBookStoreRepository;
import services.interfaces.IBookService;

import java.util.List;

public class BookService implements IBookService {
    private IBookStoreRepository bookRepo = new BookStoreRepository();

    @Override
    public List<Book> getBooks() {
        return bookRepo.getBooks();
    }

    public Book getBookByID(long bookId) {
        return bookRepo.getBookByID(bookId);
    }

    @Override
    public void addBook(Book book) {
        bookRepo.add(book);
    }


    @Override
    public void updateBook(Book book) {
       bookRepo.update(book);
    }
}
