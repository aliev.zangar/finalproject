package services.interfaces;

import domain.models.Book;

import java.util.List;

public interface IBookService {

    List<Book> getBooks();

    Book getBookByID(long bookId);

    void addBook(Book book);

    void updateBook(Book book);

}
