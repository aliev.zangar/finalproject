package services.interfaces;

import domain.models.Book;

import java.util.List;

public interface IBasketService {
        void addBookToBasket(long bookId, long userId);
        int calculateSum(long id);
        void removeBookFromBasket(long bookId, long userId);

        void buyBooks(long id);

}
