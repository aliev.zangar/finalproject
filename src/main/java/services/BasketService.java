package services;

import domain.models.Book;
import repositories.BookStoreRepository;
import repositories.interfaces.IBookStoreRepository;
import services.interfaces.IBasketService;

import java.util.List;

public class BasketService implements IBasketService {
    private IBookStoreRepository bookRepo = new BookStoreRepository();
    @Override
    public void addBookToBasket(long bookId, long userId) {
        bookRepo.addBookToBasket(bookId,userId);
    }

    @Override
    public int calculateSum(long id) {
        return bookRepo.calculateSum(id);
    }

    @Override
    public void removeBookFromBasket(long bookId, long userId) {
        bookRepo.remoteBookFromBasket(bookId, userId);
    }

    @Override
    public void buyBooks(long id) {
        bookRepo.buyBooks(id);
    }

    public List<Book> getBooksFromBasket(long userId) {
        return (List<Book>) bookRepo.getBooksFromBasket(userId);
    }
}
