package repositories.interfaces;

import domain.models.Book;

import java.util.List;


public interface IBookStoreRepository extends IEntityRepository<Book>{

    List<Book> getBooks();

    Book getBookByID(long id);

    List<Book> getBooksFromBasket(long userId);

    void addBookToBasket(long bookId, long userId);

    void remoteBookFromBasket(long bookId, long userId);

    int calculateSum(long id);

    void buyBooks(long id);

}
