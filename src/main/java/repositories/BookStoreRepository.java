package repositories;

import domain.models.Book;
import repositories.interfaces.IBookStoreRepository;
import repositories.interfaces.IDBRepository;

import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BookStoreRepository implements IBookStoreRepository {
    private IDBRepository dbrepo = new PostgresRepository();

    @Override
    public void add(Book entity) {
        try {
            String sql = "INSERT INTO book(title, author, description, genre, price, graduateyear) " +
                    "VALUES(?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, entity.getTitle());
            stmt.setString(2, entity.getAuthor());
            stmt.setString(3, entity.getDescription());
            stmt.setString(4,entity.getGenre());
            stmt.setInt(5, entity.getPrice());
            stmt.setInt(6, entity.getGraduateyear());
            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void update(Book entity) {
        String sql = "UPDATE book " +
                "SET ";
        int c = 0;
        if (entity.getTitle() != null) {
            sql += "title=?, "; c++;
        }
        if (entity.getAuthor() != null) {
            sql += "author=?, "; c++;
        }
        if (entity.getDescription() != null) {
            sql += "description=?, "; c++;
        }
        if (entity.getGenre() != null) {
            sql += "genre=?, "; c++;
        }
        if (entity.getPrice() != 0) {
            sql += "price=?, "; c++;
        }
        if (entity.getGraduateyear() != 0) {
            sql += "graduateyear=?, "; c++;
        }

        sql = sql.substring(0, sql.length() - 2);

        sql += " WHERE title = ?";

        try {
            int i = 1;
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            if (entity.getAuthor() != null) {
                stmt.setString(i++, entity.getAuthor());
            }
            if (entity.getDescription() != null) {
                stmt.setString(i++, entity.getDescription());
            }
            if (entity.getGenre() != null) {
                stmt.setString(i++, entity.getGenre());
            }
            if (entity.getPrice() != 0) {
                stmt.setInt(i++, entity.getPrice());
            }
            if (entity.getGraduateyear() != 0) {
                stmt.setInt(i++, entity.getGraduateyear());
            }
            stmt.setString(i++, entity.getTitle());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
    }

    @Override
    public void remove(Book entity) {

    }

    @Override
    public List<Book> query(String sql) {

        try {

        Statement stmt = dbrepo.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        LinkedList<Book> books = new LinkedList<>();
        while (rs.next()) {
            Book book = new Book(
                    rs.getLong("bookId"),
                    rs.getString("title"),
                    rs.getString("author"),
                    rs.getString("description"),
                    rs.getString("genre"),
                    rs.getInt("price"),
                    rs.getInt("graduateyear")
            );
            books.add(book);
        }
        return books;
    } catch (SQLException ex) {
        throw new BadRequestException("Cannot run SQL statement: " + ex.getSQLState());
    }
}

    @Override
    public Book queryOne(String sql) {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return new Book(
                        rs.getLong("bookId"),
                        rs.getString("title"),
                        rs.getString("author"),
                        rs.getString("description"),
                        rs.getString("genre"),
                        rs.getInt("price"),
                        rs.getInt("graduateyear")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }

    @Override
    public List<Book> getBooks() {
        String sql = "SELECT * FROM book";
        return query(sql);
    }

    public Book getBookByID(long id) {
        String sql = "SELECT * FROM book WHERE bookid = " + id;
        return queryOne(sql);
    }


    public List<Book> getBooksFromBasket(long userId) {
       ArrayList<Book> books = new ArrayList<Book>();
        try {
            String sql = "SELECT id,userid FROM basket WHERE userid = " + userId;
            Statement stmt = dbrepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                Book book = new Book(
                        rs.getLong("bookId"),
                        rs.getString("title"),
                        rs.getString("author"),
                        rs.getString("description"),
                        rs.getString("genre"),
                        rs.getInt("price"),
                        rs.getInt("graduateyear")
                );
                books.add(book);
            }
            return books;
        } catch (SQLException ex){
            throw new BadRequestException();
        }
    }

    @Override
    public void addBookToBasket(long bookId, long userId) {
        try {
            String sql = "INSERT INTO basket (bookid, userid) VALUES (?, ?)";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1, bookId);
            stmt.setLong(2, bookId);
            stmt.execute();
        } catch(SQLException e){
            throw new BadRequestException("Could not run SQL statement: " + e.getSQLState());
        }

    }

    @Override
    public void remoteBookFromBasket(long bookId, long userId) {
        try {
            String sql = "DELETE FROM basket WHERE bookid = ? AND userid = ?";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1, bookId);
            stmt.setLong(2, userId);
            stmt.execute();
        } catch(SQLException e){
            throw new BadRequestException("Could not run SQL statement: " + e.getSQLState());
        }

    }

    @Override
    public int calculateSum(long id) {
        List<Book> books = getBooksFromBasket(id);
        int sum = 0;
        for (int i = 0; i<books.size(); i++){
            sum += books.get(i).getPrice();
        }
        return sum;
    }

    @Override
    public void buyBooks(long id) {
        try {
            String sql = "DELETE FROM basket WHERE  userid = ?";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setLong(1, id);
            stmt.execute();
        } catch(SQLException e){
            throw new BadRequestException("Could not run SQL statement: " + e.getSQLState());
        }


    }

}
