package controllers;

import domain.models.Book;
import filters.customAnnotation.JWTTokenNeeded;
import filters.customAnnotation.OnlyAdmin;
import services.BasketService;
import services.BookService;
import services.interfaces.IBasketService;
import services.interfaces.IBookService;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("books")
public class BookController {
    private IBookService bookService = new BookService();
    private IBasketService basketService = new BasketService();

    @GET
    public Response getBooks(){
        List<Book> books;
        try {
            books = bookService.getBooks();
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity(ex.getMessage()).build();
        }

        return Response
                .status(Response.Status.OK)
                .entity(books)
                .build();
    }

    @GET
    @Path("/{id}")
    public Response getBookByID(@PathParam("id") long id){
        Book book;
        try {
            book = bookService.getBookByID(id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity(ex.getMessage()).build();
        }
        if (book == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Not found!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(book)
                    .build();
        }
    }


    @OnlyAdmin
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/addbook")
    public Response addNewBook(Book book) {

        try {
            bookService.addBook(book);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("Book added successfully!")
                .build();
    }

    @OnlyAdmin
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/updatebook")
    public Response updateBook(Book book, @Context ContainerRequestContext requestContext) {
        if (!requestContext.getSecurityContext().isUserInRole("admin") &&
                !requestContext.getSecurityContext().getUserPrincipal().getName().equals(book.getTitle())) {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .build();
        }

        try {
            bookService.updateBook(book);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.CREATED)
                .entity("User updated successfully!")
                .build();
    }


    @JWTTokenNeeded
    @POST
    @Path("{id}/addtobasket")
    public Response addBookToBasket(@PathParam("id") long bookId, @Context ContainerRequestContext requestContext){
        long userId = Long.parseLong(requestContext.getSecurityContext().getUserPrincipal().getName());

        try {
            basketService.addBookToBasket( bookId , userId);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity(ex.getMessage()).build();
        }
        return Response
                .status(Response.Status.OK)
                .entity("Successfully added to basket.")
                .build();
    }

}
