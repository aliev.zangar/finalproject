package domain.models;

import java.io.Serializable;
import java.util.List;

public class Basket implements Serializable {
    private long bookId;
    private long userId;
    private List<Book> basket;

    public Basket() {
    }

    public Basket(Long bookId, long userId) {
        this.bookId = bookId;
        this.userId = userId;
    }

    public long getBookId() {
        return bookId;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}

