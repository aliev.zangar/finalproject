package domain.models;

public class Book {
    private long bookId;
    private String title;
    private String author;
    private String description;
    private String genre;
    private int price;
    private int graduateyear;

    public Book() {
    }

    public Book(long bookId, String title, String author, String description,String genre, int price, int graduateyear) {
        this.bookId = bookId;
        this.title = title;
        this.author = author;
        this.description = description;
        this.price = price;
        this.graduateyear = graduateyear;
    }

    public long getBookId() {
        return bookId;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getGraduateyear() {
        return graduateyear;
    }

    public void setGraduateyear(int graduateyear) {
        this.graduateyear = graduateyear;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return "book{" +
                "id='" + bookId + '\'' +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", price='" + price + '\'' +
                ", graduateyear=" + graduateyear +
                '}';
    }
}
